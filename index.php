<?php
/**
 * Git bundle patcher
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 * @version 0.74
 */

// *** Settings ***
define('VERSION','0.74');
$appName    = 'Git Bundle Patcher';

// Projects directory:
$baseDir    = preparePath(dirname(__FILE__).'/..');
// Git Bundles directory:
$bundlesDir = preparePath(dirname(__FILE__).'/../git-bundles');

// *** Tools **
// Windows:
// $gitExe = "\"C:\\Program Files\\Git\\bin\\git\""; 
// $tarExe = "\"C:\\Program Files\\Git\\bin\\sh.exe\" --login -c \"tar"; 
// Linux:
$gitExe = "git"; 
$tarExe = "tar"; 

// Do we have a local config file?
if(file_exists(dirname(__FILE__).'/config.php'))
    require dirname(__FILE__).'/config.php';

// Check paths
if(!is_dir($baseDir))
    die("<h1>Error!</h1> baseDir ".(empty($baseDir) ? '(empty)' : $baseDir)." doesn't exist. Check your configuration.");
if(!is_dir($bundlesDir))
    die("<h1>Error!</h1> bundlesDir ".(empty($bundlesDir) ? '(empty)' : $bundlesDir)." doesn't exist. Check your configuration.");

$output = '';
$message = '';
$errors = array();
$postCmd = isset($_POST['cmd']) ? $_POST['cmd'] : null;
if(!is_null($postCmd)) {
    // echo par($_POST);
    // echo par($_SERVER);
    // echo "<li>getcwd: ".getcwd();
    // chdir($_POST['project']);
    // echo "<li>getcwd: ".getcwd();
    // $cmd = 'set PATH='.$gitExe.';%PATH%';
    $cmd = null;
    switch($postCmd)
    {
        case 'newdir': // {{{ 
            if(!isset($_POST['newdir']))
                $errors['project'][] = 'Please enter a new directory';
            $cmd = 'cd '.$baseDir.' && mkdir '.$_POST['newdir'];
            break; // }}} 
        case 'clone': // {{{
            if(!isset($_POST['project']) and !isset($_POST['newdir']))
                $errors['project'][] = 'Please either select a project or enter a new directory';
            else {
                if(isset($_POST['project']))
                    $project = str_replace("\\", '/', $_POST['project']);
                if(isset($_POST['newdir']))
                    $project = $_POST['project'] = $_POST['newdir'];
            }
            if(!isset($_POST['bundle']))
                $errors['bundle'][] = 'Please select a bundle file';
            list($ts, $bundle) = explode('|||', $_POST['bundle'] );
            $cmd = 'cd '.$baseDir.' && '.$gitExe.' clone '.$bundle.' -v -b master '.$project;
            
            break; // }}} 
        case 'init': // {{{ 
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            $cmd = 'cd '.str_replace("\\", '/', $_POST['project']).' && '.$gitExe.' init';
            break; // }}} 
        case 'upload': // {{{ 
            if($_FILES['file']['error']==UPLOAD_ERR_NO_FILE)
                $errors['file'][] = 'Please select a bundle file';
            if($_FILES['file']['error']==UPLOAD_ERR_OK) {
                $name = $_FILES['file']['name'];
                $tmp_name = $_FILES['file']['tmp_name'];
                // Delete already existing bundle first
                if(is_file($bundlesDir.'/'.$name))
                    @unlink($bundlesDir.'/'.$name);
                move_uploaded_file($tmp_name, $bundlesDir.'/'.$name);
                header("X-Php-Git-Bundle: cmd={$postCmd},result=OK");
                $message .= '<b>Upload Bundle</b><br />The bundle file has been uploaded.';
            } else {
                header("X-Php-Git-Bundle: cmd={$postCmd},result=NOK");
                $message .= "<b>Upload Bundle</b><br />Error: Couldn't upload the bundle file.";
            }
            $postCmd = 'verify';
            break; // }}} 
        case 'backup': // {{{ 
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            // DEBUG echo "<li>project: ".$_POST['project'];
            $filename = !empty($_POST['backupfilename']) ? $_POST['backupfilename'] : basename($_POST['project']);
            $filename .= '_'.date("Y-m-d_Hi").'.zip';
            $filename = '../'.$filename;
            $project = str_replace(array("\\","C:/"),array('/','/c/'),$_POST['project']).'/';
            $project = $_POST['project'].DIRECTORY_SEPARATOR;
            // DEBUG echo "<li>filename: $filename";
            // DEBUG echo "<li>project: $project";
            
            // increase script timeout value
            ini_set('max_execution_time', 300);

            // create object
            $zip = new ZipArchive();

            // open archive
            if ($zip->open($filename, ZIPARCHIVE::CREATE) !== TRUE) {
                die ("Could not open archive");
            }

            // initialize an iterator
            // pass it the directory to be processed
            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($project));

            // iterate over the directory
            // add each file found to the archive
            foreach ($iterator as $key=>$value) {
                // DEBUG echo "<li>$key: ".realpath($key)." => $value";
                $zip->addFile(realpath($key), basename($key)) or die ("ERROR: Could not add file: $key");
            }

            // close and save archive
            $zip->close();
            echo "<li>Archive <em>$filename</em> created successfully.";

            break; // }}} 
        case 'verify': // {{{
           if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            if(!isset($_POST['bundle']))
                $errors['bundle'][] = 'Please select a bundle file';
            chdir($_POST['project']);
            list($ts, $bundle) = explode('|||', $_POST['bundle'] );
            $cmd .= $gitExe.' bundle verify '.$bundle;
            $postCmd = 'pull'; 
            break; // }}}
        case 'status': // {{{ 
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            chdir($_POST['project']);
            $cmd .= $gitExe.' status';
            $postCmd = 'status'; 
            break; // }}}
        case 'pull': // {{{ 
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            if(!isset($_POST['bundle']))
                $errors['bundle'][] = 'Please select a bundle file';
            chdir($_POST['project']);
            list($ts, $bundle) = explode('|||', $_POST['bundle'] ); 
            $cmd .= $gitExe.' pull --tags -v '.$bundle.' master';
            // Set next postCmd
            $postCmd = 'verify';
            break; // }}} 
        case 'log': // {{{ 
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            $cmd = 'cd '.$_POST['project'].' && '.$gitExe.' log -v ';
            if(isset($_POST[$_POST['cmd']])) {
                foreach($_POST[$_POST['cmd']] as $switch)
                    $cmd .= ' '.$switch;
            }
            break; // }}}
        case 'ls-remote': // {{{
            if(!isset($_POST['bundle']))
                $errors['bundle'][] = 'Please select a bundle file';
            list($ts, $bundle) = explode('|||', $_POST['bundle'] );
            $cmd .=  $gitExe.' ls-remote '.$bundle;
            if(isset($_POST[$_POST['cmd']])) {
                foreach($_POST[$_POST['cmd']] as $switch)
                    $cmd .= ' '.$switch;
            }
            break; // }}}
        case 'cmdline': // {{{
            if(!isset($_POST['project']))
                $errors['project'][] = 'Please select a project';
            else
                chdir($_POST['project']);
            if(empty($_POST['cmdline']['itxt']['cmd']))
                $errors['__all__'][] = 'Please enter a command';
            else {
                if(substr($_POST['cmdline']['itxt']['cmd'], 0, 4)=='git ')
                    $_POST['cmdline']['itxt']['cmd'] = str_replace('git ', $gitExe.' ', $_POST['cmdline']['itxt']['cmd']);
                $cmd .= $_POST['cmdline']['itxt']['cmd'];
            }
            $postCmd = 'cmdline'; 
            break; // }}} 
        default: // {{{ 
            $errors['__all__'][] = 'Command not supported.';
            break; // }}} 
    }
    if(!is_null($cmd))
        $cmd .= ' 2>&1';
    if(count($errors)==0 and !is_null($cmd)) {
        $output = '$ '.$cmd."\n\n".shell_exec($cmd);
        $output = str_replace("cd ".$baseDir.' && ','',$output);
        if(isset($_POST['project']))
            $output = str_replace("cd ".$_POST['project'].' && ','',$output);
        $output = str_replace($gitExe, 'git', $output);
        $output = str_replace(' 2>&1', '', $output);
    } else {
        unset($_POST['cmd']);
    }
}

$commands = getCommands();
// Get projects dirs
$projects = getProjectDirs($baseDir);
// get git bundles
$bundles = getBundles($bundlesDir);

$form = renderForm(array('commands'=>$commands,'projects'=>$projects,'output'=>$output,'bundles'=>$bundles,'errors'=>$errors));
renderPage(array('message'=>$message,'content'=>$form));

// {{{ par
function par($data, $title=null)
{
    ob_start();
    echo '<pre style="max-height:200px;border:1px solid #aaa;padding: 8px; overflow:auto;">';
    if(!is_null($title))
        echo '<strong>'.$title.'</strong>'."\r\n";
    print_r($data);
    echo '</pre>';
    $cts = ob_get_contents();
    ob_end_clean();
    return $cts;
} // }}} 
// {{{ getProjectDirs
function getProjectDirs($baseDir)
{
    Global $bundlesDir;
    // DEBUG echo "<li>baseDir: $baseDir";
    // DEBUG echo "<li>bundlesDir: $bundlesDir";
    $ownDir = str_replace("\\",'/',dirname(__FILE__));
    $subdirs = explode('/',$ownDir);
    // DEBUG echo par($subdirs);
    $ownSubdir = $subdirs[count($subdirs)-1];
    $result = array();
    $d = dir($baseDir);
    // echo "Handle: " . $d->handle . "\n";
    // echo "Path: " . $d->path . "\n";
    while (false !== ($entry = $d->read())) {
        // echo "<li>entry: $entry";
        if($entry<>'.' and $entry<>'..' and is_dir(preparePath($baseDir.'/'.$entry)) and $entry!==$ownSubdir and $entry!==basename($bundlesDir))
            $result[preparePath($baseDir.'/'.$entry)] = $entry;
    }
    $d->close();
    asort($result);
    return $result;
} // }}} 
// {{{ getBundles
function getBundles($bundlesDir)
{
    Global $baseDir;
    $projects = array_values(getProjectDirs($baseDir));
    // DEBUG echo par($projects, '$projects');
    $result = array();
    $d = dir($bundlesDir);
    //echo "Handle: " . $d->handle . "\n";
    //echo "Path: " . $d->path . "\n";
    while (false !== ($entry = $d->read())) {
        //echo "<li>entry: $entry"A
        if(substr($entry,-4)=='.bdl' or substr($entry,-7)=='.bundle') {
            foreach($projects as $project) {
                if(substr($entry, 0, strlen($project))==$project)
                    $result[$project][filemtime($bundlesDir.'/'.$entry)] = preparePath($bundlesDir.'/'.$entry).'|||'.$entry;
            }
        }
    }
    $d->close();
    // sort by projects
    ksort($result);
    // DEBUG echo par($result, '$result');
    foreach($result as $project=>$bundles)
        krsort($result[$project]);
    // DEBUG echo par($result, '$result 2');
    $result2=array();
    foreach($result as $project=>$bundles) {
        foreach($bundles as $ts=>$data) {
            list($path, $entry)=explode('|||', $data);
            $result2[$ts.'|||'.$path] = $entry;
        }
    }
    // asort($result2);
    // DEBUG echo par($result2, '$result2');
    return $result2;
} // }}} 
// {{{ getCommands
function getCommands()
{
    return array(
        'newdir'    => array(
            'title'=>'Create a new project directory',
        ),
        'clone'     => array(
            'title'=>'Clone Git repository from bundle',
        ),
        /*
        'init'      => array(
            'title'=>'Init Git repository',
        ),
         */
        'upload'    => array(
            'title'=>'Upload a Git bundle',
        ),
        'backup'    => array(
            'title'=>'Backup a project dir.',
        ),
        'verify'    => array(
            'title'=>'Verify a Git bundle',
        ),
        'status'    => array(
            'title'=>'Show Repository Status',
        ),
        'pull'      => array(
            'title'=>'Apply a Git bundle',
        ),
        'log'       => array(
            'title'=>'Show log information',
            'switches'=>array(
                '--oneline'     => array(
                    'title' => 'One Line',
                    'on' => 1,
                ),
                '--decorate'    => array(
                    'title' => 'Decorate',
                    'on' => 1,
                ),
                '--graph'       => array(
                    'title' => 'Graph',
                    'on' => 1,
                ),
                '--all'         => array(
                    'title' => 'All',
                    'on' => 1,
                ),
            ),
        ),
        'ls-remote' => array(
            'title'=>'Show Bundle information',
            'switches'=>array(
                '--heads' => array(
                    'title'=>'Limit to only refs/heads'
                ),
                '--tags'=>array(
                    'title'=>'Limit to only refs/tags',
                ),
            ),
        ),
        'cmdline' => array(
            'title'=>'Command',
            'itxt'=>array(
                'cmd'=>array(
                    'title'=>'Command'
                ),
            ),
        ),
    );
} // }}} 
// {{{ preparePath
function preparePath($path)
{
    $path = realpath(str_replace("\\",'/',$path));
    return $path;
} // }}} 
// {{{ renderForm
function renderForm($data, $return=true)
{
    Global $postCmd;
    if(!is_null($data))
        extract($data);
    ob_start();
    echo <<< EOL
        <form method="post" action="{$_SERVER['PHP_SELF']}" enctype="multipart/form-data">
        <div class="container">
EOL;
    if(count($errors)>0) {
        // {{{ Show errors
        echo <<< EOL
            <div class="row">
                <div class="col-sm-12 error">
                <b>Errors</b>
                <ul>
EOL;
        foreach($errors as $field=>$fieldErrors) {
            foreach($fieldErrors as $errorMsg) {
                echo '<li>'.$errorMsg.'</li>';
            }
        }
        echo <<< EOL
                </ul>
                </div>
            </div>
EOL;
        // }}} 
    }
    if(trim($output)!=='') {
        echo <<< EOL
        <div class="row">
        <div class="col-sm-4 col-md-8 col-lg-12">
        <fieldset>
        <legend>Output:</legend>
        <pre id="output" class="output">$output</pre>
        </fieldset>
        </div>
        </div>
EOL;
    }
    // {{{ Commands
    echo <<< EOL
        <div class="row">
            <div class="col-sm-12 col-lg-3">
            <fieldset>
                <legend>Command:</legend>
EOL;
    $i=0;
    foreach($commands as $command=>$settings) {
        echo "\t\t\t\t".'<input name="cmd" type="radio" value="'.$command.'" onclick="toggle(this.value);" ';
        echo $postCmd==$command ? 'checked="checked"' : '';
        echo ' /> '.$settings['title']."<br/>\n";
        if(isset($settings['switches'])) {
            echo '<div class="cmd-switches '.$command.'" style="display:none">'."\n";
            $k=0;
            foreach($settings['switches'] as $switch=>$switchSettings) {
                $switchOn = '';
                if(isset($switchSettings['on']))
                    $switchOn = 'checked="checked" ';
                echo "<input type=\"checkbox\" name=\"{$command}[$k]\" value=\"$switch\" $switchOn/> {$switchSettings['title']}<br/>\n";
                $k++;
            }
            echo "</div>\n";
        }
        if(isset($settings['itxt'])) {
            echo '<div class="cmd-itxt '.$command.'" style="display:none">'."\n";
            foreach($settings['itxt'] as $itxt=>$itxSettings) {
                $value = !empty($_POST[$command]['itxt'][$itxt]) ? $_POST[$command]['itxt'][$itxt] : '';
                echo "{$itxSettings['title']}:&nbsp;<input type=\"text\" name=\"{$command}[itxt][$itxt]\" value=\"{$value}\" /><br/>\n";
            }
            echo "</div>\n";
        }
        $i++;
    }
    echo <<< EOL
            </fieldset> 
            </div>
EOL;
    // }}}
    // {{{ Projects
    echo <<< EOL
        <div class="col-sm-12 col-lg-3 projects newdir clone init backup pull status log verify cmdline" style="display:none;">
        <fieldset>
            <legend>Project:</legend>
            <div style="max-height:300px;overflow:auto">
EOL;
    if(empty($_POST['project'])) $_POST['project']=null;
    foreach($projects as $key=>$value) {
        $key = str_replace("\\", '/', $key);
        echo "\t\t\t\t".'<input name="project" type="radio" value="'.$key.'" ';
        echo str_replace("\\", '/', $_POST['project'])==$key ? 'checked="checked"' : '';
        echo '/> '.$value."<br/>\n";
    }
    echo <<< EOL
            </div>
EOL;
    if(isset($_POST['project']))
        echo "<div><small>Selected: <b>".basename($_POST['project'])."</b></small></div>";
    echo <<< EOL
            <label for="newdir">New Dir:</label>
            <input type="text" name="newdir" />
        </fieldset>
        </div>
EOL;

    // }}}
    // {{{ Upload Bundle File
    echo <<< EOL
            <div class="col-sm-12 col-lg-3 cmd upload" style="display:none"> 
            <fieldset>
                <legend>Bundle File:</legend>
                <input name="file" type="file" />
            </fieldset>
            </div>
EOL;
    // }}} 
    // {{{ Backup Filename
    echo <<< EOL
            <div class="col-sm-12 col-lg-3 cmd backup" style="display:none">
            <fieldset>
                <legend>Backup Filename:</legend>
                <input name="backupfilename" type="text" /> .tar.gz (will be added)
            </fieldset>
            </div>
EOL;
    // }}} 
    // {{{ Bundles
    if(empty($_POST['bundle'])) $_POST['bundle']=null;
    echo <<< EOL
            <div class="col-sm-12 col-lg-3 cmd clone pull ls-remote verify" style="display:none;">
            <fieldset>
                <legend>Git Bundle:</legend>
                <div style="max-height:300px;overflow:auto">
EOL;
    foreach($bundles as $key=>$value) {
        $key = str_replace("\\", '/', $key);
        echo "\t\t\t\t".'<input name="bundle" type="radio" value="'.$key.'" ';
        echo str_replace("\\", '/', $_POST['bundle'])==$key ? 'checked="checked"' : '';
        echo '/> <span>'.$value."</span><div style=\"clear:left\"></div>\n";
    }
    echo <<< EOL
                </div>
EOL;
    if(isset($_POST['bundle']))
        echo "<small>Selected: <b>".basename($_POST['bundle'])."</b></small>";
    echo <<< EOL
            </fieldset>
            </div>
        </div>
EOL;
    // }}}
    echo <<< EOL
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <button class="primary" type="submit" onclick="if(confirm('Really run the selected command?')==false) return false;">Run Command</button>
                </div>
            </div>
            </div>
        </form>
EOL;
    $cts = ob_get_contents();
    ob_end_clean();
    if($return)
        return $cts;
    echo $cts;
} // }}} 
// {{{ renderPage
function renderPage($data=null)
{
    Global $postCmd;
    if(!is_null($data))
        extract($data);
    $version = VERSION;
    echo <<< EOL
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/mini-default.min.css">
        <title>{$GLOBALS['appName']}</title>
        <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
        <style type="text/css">
        <!--
        form { background-color: #f8f8f8; border: none; }
        div .errors {padding:5px;border:1px solid red;background-color:#ffaa64}
        pre.output {max-width: 95%; border:1px solid #aaa;height:200px;overflow:auto;margin:15px;padding:8px;background-color:#222;color:lime;}
        -->
        </style>
        <script type="text/javascript">
        <!--
function toggle(cmd) // {{{
{ 
    $(".cmd").hide(400);
    $(".cmd-switches").hide(400);
    $(".cmd-itxts").hide(400);
    $(".projects").hide(400);
    $("."+cmd).show(400);
    $(".cmd-switches ."+cmd).show(400);
    $(".cmd-itxt ."+cmd).show(400);
    $(".projects ."+cmd).show(400);
} // }}} 
function init() // {{{
{
EOL;
    if(!is_null($postCmd)) {
        echo <<< EOL
    toggle('{$postCmd}');
EOL;
    } 
    echo <<< EOL

}// }}} 
        // -->
        </script>
        <script type="text/javascript">
$(function() {
    $('input[name=project]').on('click', function() {
        var project = $(this).val();
        var parts = project.split("/");
        project = parts[parts.length-1];
        console.log("project: "+project);
        var \$radios = $('input:radio[name=bundle]');
        \$radios.each(function() {
            var bundle = $(this).val();
            console.log("bundle: "+bundle);
            if(bundle.indexOf(project)>-1) {
                console.log("Found");
                $(this).show();
                $(this).next('span').show();
                $(this).next('div').show();
            } else {
                $(this).hide();
                $(this).next('span').hide();
                $(this).next('div').hide();
            }
        });
    });
});
        // -->
        </script>
    </head>
    <body onload="init();">
        <header class="sticky">
            <a title="Re-load page" class="logo" href="{$_SERVER['PHP_SELF']}">{$GLOBALS['appName']}</a>
        </header>
EOL;
    if($message!=='') :
        echo <<< EOL
        <span class="toast">$message</span>
EOL;
    endif;
    $year = date('Y');
    echo <<< EOL
        $content
        <footer class="sticky">
            <p>
                {$GLOBALS['appName']} V{$version} | &copy; 2012-{$year} by <a href="http://www.diggin-data.de">Diggin' Data</a>
            </p>
        </footer>
    </body>
</html>
EOL;
} // }}} 
/* vim:set ai sw=4 sts=4 et fdm=marker fdc=4: */ 
?>
