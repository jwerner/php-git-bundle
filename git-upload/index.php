<?php
/**
 * Git Bundle Uploader
 *
 * Receives uploaded Git bundle files via POST
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 * @version 0.1
 */

$upload_max_filesize    = return_bytes(ini_get('upload_max_filesize'));
$post_max_size          = return_bytes(ini_get('post_max_size'));

ob_start();
echo "upload_max_filesize: $upload_max_filesize bytes<br />";
echo "post_max_size: $post_max_size bytes<br />";

if($post_max_size<$upload_max_filesize)
    $max_size = floor($post_max_size*0.9);
else
    $max_size = $upload_max_filesize;
echo "max_size: $max_size bytes<br />";

// Settings
$basedir            = str_replace("\\",'/',realpath(dirname(__FILE__).'/..'));
$default_destdir    = 'git-bundles';
$user               = 'YOUR_USERNAME';
$password           = 'YOUR_USERPASSWORD';

echo "basedir: $basedir<br />";

$cts = ob_get_clean();

// Handle file upload
if(count($_POST)>0)
{
    // User & Password
    if(!isset($_POST['username']) or $_POST['username']=='')
        die("Username not set");
    if($_POST['username']<>$username and $_POST['password']<>$password)
        die("Username and Password don't match");
    if(empty($_POST['destdir']))
        die("destdir not set");
    $uploaddir = str_replace("\\",'/',realpath($basedir.'/'.$_POST['destdir']));
    if(!is_dir($uploaddir))
        die("Invalid directory: <em>$uploaddir</em>");
    $uploadfile = $uploaddir . '/' . basename($_FILES['userfile']['name']);
    if(is_file($uploadfile) and $_POST['overwrite']!=='1') 
        die("File already exists: <em>$uploadfile</em>");
    else
        @unlink($uploadfile);

    $fp = fopen("log.txt", "ab");
    fwrite($fp, date("Y-m-d H:i:s")."\t".$_SERVER['REMOTE_ADDR']."\t".$_FILES['userfile']['name']."\r\n");
    fclose($fp);

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        echo "SUCCESS";
    } else {
        echo "Possible file upload attack!\n";
    }
}

if(isset($_GET['showForm'])) {
?>
<html>
<head>
<title>Git Bundle Uploader</title>
</head>
<body>
    <h1>Git Bundle Uploader</h1>
    <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
    <!-- MAX_FILE_SIZE must precede the file input field -->
    <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_size ?>" />

    Username: <input name="username" size="10" type="text" /><br />

    Password: <input name="password" size="10" type="password" /><br />

    <!-- Name of input element determines name in $_FILES array -->
    File: <input name="userfile" type="file" /><br />

    Directory: <input name="destdir" type="text" value="<?php echo $default_destdir; ?>" /><br />

    Overwrite: <input type="checkbox" name="overwrite" value="1" /><br />

    <input type="submit" value="Send File" />
    </form>
</body>
</html>
<?php
}

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 fdm=marker fdc=4: */
?>
