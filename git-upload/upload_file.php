<?php
/**
 * Sample to upload a Git bundle via POST
 *
 * @authore Joachim Werner <joachim.werner@diggin-data.de>
 */

// File to upload
$upload_file = 'C:/xampp/htdocs/dd/git-upload/uploadtest.txt';

// User & Password (@see index.php)
$username = 'YOUR_USERNAME';
$password = 'YOUR_USERPASSWORD';

// Additional switches
$destdir = 'git-bundles';
$overwrite = 1;
$url = 'http://deruewdlxa00584/dd/git-upload/index.php';
$postVars = array(
    'username'=>$username,
    'password'=>$password,
    'destdir'=>$destdir,
    'overwrite'=>$overwrite,
);
$boundary = '--boundary'.uniqid();
$content = $boundary."\r\n"
    . "Content-Disposition: form-data; name=\"userfile\"; filename=\"".basename($upload_file)."\"\r\n"
    . "Content-Type: ".mime_content_type($upload_file)."\r\n\r\n"
    . file_get_contents($upload_file) . "\r\n"
    . $boundary;
foreach($postVars as $key=>$value) {
    $content .= "\r\n"
        . "Content-Disposition: form-data; name=\"".$key."\"\r\n\r\n"
        . $value . "\r\n"
        . $boundary;
}
$content .= '--';
/* DEBUG
print_r($content);
echo "</pre>";
die;
 */

$context = stream_context_create(
    array(
        "http" => array(
            "method" => "POST",
            "header" => "Connection: keep-alive\r\n"
                ."Content-Type: multipart/form-data; boundary=".$boundary."\r\n",
            "content" => $content
        )
    )
);

$html = file_get_contents($url, false, $context);
if($html===false)
    echo "ERROR";
else
    echo $html;

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4 fdm=marker fdc=4: */
?>
