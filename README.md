# Overview

**php-git-bundle** is a small PHP application to patch web applications on a shared host using [Git bundles][git-bundle].

Using Git is nice if you have a console (e.g. via SSH) available, but on shared web hosters somethimes you don't have one.

But perhaps you can run console commands via [PHP][php-shell-exec]?

# Install

Get a copy of **php-git-bundle**, e.g.

    git clone https://bitbucket.org/jwerner/php-git-bundle.git

Check the settings in `index.php`:

    // Projects directory:
    $baseDir    = preparePath(dirname(__FILE__).'/../..');
    // Git Bundles directory:
    $bundlesDir = preparePath(dirname(__FILE__).'/../../git-bundles');

    // *** Tools **
    // Linux:
    $gitExe = "git"; 
    $tarExe = "tar"; 

Upload the `php-git-bundle` to your webserver.

# Usage

## Create a Git Bundle

On you local machine, create a Git bundle:

    git bundle create path/to/file.bundle master

In order to make it easy to later update the other repository with an incremental bundle,  
you can use a tag to remember up to what commit you last processed:

    git tag -f lastbundle master

## Create a new Project Directory

1. On your shared host, open the _php-git-bundle_ application.
2. Enter the new project sub-directory name into the field **New Dir** in the Project section
3. Click on **[Run Command]** and confirm

The new directory will be created below your _bundlesDir_ (see section _Install_ above).

## Upload the Bundle to the Shared Host

1. In _php-git-bundle_, click on **Upload a Git bundle**
2. Select the **Bundle File**
3. Click on **[Run Command]**

The bundle file will be uploaded and is now available to initialize a project or
for a project to pull from.

## Initialize the Shared Host Web Application

1. On your shared host, open the php-git-bundle application.
2. Click on **Clone Git repository from bundle**
3. Select a **Project** directory
4. Select a **Bundle File**
4. Click on **[Run Command]**

## Apply a Bundle

1. On your shared host, open the _php-git-bundle_ application.
2. Click on **Apply a Git bundle**
3. Select a **Project** directory
4. Select a **Bundle File**
5. Click on **[Run Command]**

The selected bundle will be applied to the project.

## Creating Update Bundles

* Create a new bundle since the lastbundle tag
* Set the _lastbundle_ tag to the current commit

    git bundle create YOURPROJECTNAME-VERSION.bundle lastbundle..master --branches --tags
    git tag -f lastbundle master

* Upload the bundle (see section **Upload the Bundle...**)
* Apply the bundle to the project (see section **Apply a Bundle**)

## Additional Commands

   TO DO


[git-bundle]: http://git-scm.com/docs/git-bundle
[php-shell-exec]: http://www.php.net/shell_exec
